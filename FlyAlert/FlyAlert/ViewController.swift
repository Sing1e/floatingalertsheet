//
//  ViewController.swift
//  FlyAlert
//
//  Created by Alexandr Shevchenko on 09.10.2020.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func buttonTap(_ sender: Any) {
        var array = [FloatingAlertAction]()
        array.append(.title(icon: UIImage.checkmark, title: "This is Title"))
        array.append(.separator)
        array.append(.actionArrow(icon: UIImage.add, title: "add item", action: {
            print("add item")
        }))
        array.append(.actionArrow(icon: UIImage.actions, title: "this is action", action: {
            print("this is action")
        }))
        array.append(.actionSwitch(title: "Вот тут должен быть очень большой тайтл, пиздец просто какой большой", action: { (uiSwitch) in
            print(uiSwitch.isOn)
        }))
        let action = FloatingAlertAction.action(icon: UIImage.remove, title: "this is remove", action: {
            print("this is remove")
        })
        array.append(action)

        let alertController = FloatingAlertController(nibName: "FloatingAlertController", bundle: nil)
        alertController.modalPresentationStyle = .overCurrentContext
        alertController.backgroundColor = .white
        alertController.cornerRadius = 10 
        alertController.floatingAlert = array
//        alertController.transitioningDelegate
//        alertController.transitionCoordinator
        present(alertController, animated: false, completion: nil)
    }
}
